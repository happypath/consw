import { asserts as A, JSONC } from "./test_deps.ts";

Deno.test("JSONC", async (t) => {
  await t.step("basic Mr Watson", () => {
    A.assertEquals(
      JSONC.parse(`// comment
      {"a": 1}`),
      { a: 1 },
    );
  });
});
